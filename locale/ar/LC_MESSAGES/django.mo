��          t      �         #        5  	   <     F     N     a  6   i     �  
   �  	   �  #  �  9   �  !        >     V     e     q  Q   �     �     �     
        
                                 	                   Activation email will be sent here. E-mail Full name Message No known userNone Subject This username is already taken. Please choose another. Username Your email Your name Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2014-08-22 23:42+0200
Last-Translator: Dhaifallah Alwadani <dalwadani@gmail.com>
Language-Team: Arabic <https://hosted.weblate.org/projects/weblate/master/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 1.10-dev
 رسالة التفعيل سيتم ارسالها هنا. البريد الإلكتروني الاسم الكامل الرسالة لا أحد العنوان اسم المستخدم محجوز من قبل. الرجاء أخيار غيره. اسم المستخدم بريدك الإلكتروني اسمك 