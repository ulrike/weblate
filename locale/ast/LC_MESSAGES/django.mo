��    *      l  ;   �      �  #   �     �     �     �       
             "     )     9  	   L  
   V     a     g  '   o     �     �     �  $   �  '        @     _  /   h     �     �     �  ?   �     �          #     ,     ?     K     S     g     t     �  
   �  	   �     �     �  �  �  .   �  )   �     	     	     	     &	     6	     E	     M	     d	     w	     �	     �	     �	  '   �	  !   �	  #   �	  &   
  .   ;
  *   j
  $   �
     �
  @   �
                 D   .     s     �     �     �     �     �     �     �               $  
   2     =     T        "                                	             !                            (   $      %   
                    #                                     '   )                       *   &                   Activation email will be sent here. At least six characters long. Change password Contact Copy Debug mode Django caching E-mail Email addresses Interface Language Languages Loading… Login Message Message has been sent to administrator. Notification on any translation Notification on new comment Notification on new contributor Notification on new language request Notification on new string to translate Notification on new suggestion Password Password needs to have at least six characters. Password reset Plural Plural form %d Repeat the password so we can verify you typed it in correctly. Reset my password Secondary languages Singular Site administrator Site domain Subject Subscribed projects User profile User registration Username Your email Your name Your profile has been updated. pyuca library Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2014-10-06 14:51+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Asturian <https://hosted.weblate.org/projects/weblate/bootstrap/ast/>
Language: ast
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 1.10-dev
 El corréu d'activación dunviarásete pequí. Polo menos seyes carauteres de llonxitú. Camudar conseña Contautu Copiar Mou depuración Cachéu Django Corréu Direiciones de corréu Llingua d'interfaz Llingues En cargando… Aniciar sesión Mensax El mensax fo dunviáu al alministrador. Notificación de cualaquier torna Notificación d'un comentariu nuevu Notificación d'un contribuyente nuevu Notificación d'una petición de llingua nueva Notificación d'una cadena nueva pa tornar Notificación d'una suxerencia nueva Conseña La conseña necesita tener polo menos 6 carauteres de llonxitú. Reaniciar conseña Plural Forma plural %d Repite la conseña pa que podamos verificar que la escribiesti bien. Reaniciar la conseña mio Llingues secundaries Singular Alministrador del sitiu Dominiu del sitiu Asuntu Proyeutos soscritos Perfil d'usuariu Rexistru d'usuariu Viesu d'usuariu El corréu to El nome to El perfil to anovóse. Llibrería pyuca 