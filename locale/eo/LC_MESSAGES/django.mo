��          |      �          #   !     E  	   L     V     ^  V   q     �  6   �       
     	     �  %  )   �  	     
   )     4     <  L   B     �  4   �  	   �     �     �                                      	   
                  Activation email will be sent here. E-mail Full name Message No known userNone Please contact us in English, otherwise we might be unable to understand your request. Subject This username is already taken. Please choose another. Username Your email Your name Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2015-01-01 18:37+0200
Last-Translator: Eliovir <eliovir@gmail.com>
Language-Team: Esperanto <https://hosted.weblate.org/projects/weblate/master/eo/>
Language: eo
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.2-dev
 Aktiviga retpoŝto estos sendita ĉi tie. Retadreso Plena nomo Mesaĝo Neniu Bonvole kontaktu nin en la angla, alikaze malfacile ni komprenos vian peton. Temo Tiu uzantnomo estas jam uzata. Bonvolu elekti alian. Uzantnomo Via retadreso Via nomo 