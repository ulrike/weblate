��          �      �           	                    $     ,  	   ?     I     Q     ^     s  %   |     �     �  
   �     �  6   �     	       1   $  
   V  	   a  �  k     B     T     g     u     �     �     �  
   �     �  &   �       }        �     �     �     �  Y        [  %   l  �   �          3                                                            	                       
                     About Download E-mail Home Hosting Interface Language Languages Message New password New password (again) Password Please check your math and try again. Project name Project website Statistics Subject This username is already taken. Please choose another. Username Username or email You can add another emails on Authentication tab. Your email Your name Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2015-02-23 11:39+0200
Last-Translator: Michal Čihař <michal@cihar.com>
Language-Team: Armenian <https://hosted.weblate.org/projects/weblate/master/hy/>
Language: hy
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Weblate 2.3-dev
 Մեր մասին Ներբեռնել Էլ-փոստ Գլխավոր Հոսթինգ Միջերեսի լեզու Լեզուներ Նամակ Նոր գաղտնաբառ Նոր գաղտնաբառ (նորից) Գաղտնաբառ Խնդրում ենք ստուգել ձեր գիտելիքները մաթեմատիկայում եւ փարձել նորից: Նախագծի անվանում Նախագծի կայք Վիճակագրություն Առարկա Այս օգտանունը զբաղված է: Խնդրում ենք ընտրել նորը: Օգտանուն Օգտանուն կամ էլ-փոստ Դուք կարող եք ավելացնել ուրիշ էլ-փոստային հասցեներ  Վավերացման սյունատում: Ձեր էլ-փոստը Ձեր անունը 