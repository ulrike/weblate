��    5      �  G   l      �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                	                              %     )     2     6     >     A     E     N  	   R     \     m     p     t     {  /   ~  8   �  ?   �     '     +     4     :     =     A     I     L  	   P  �  Z     ;     ?     E     I     Q     W     _     c     l     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     	     		  	   	     	     ,	     0	     8	     @	  .   C	  5   r	  G   �	     �	     �	     �	     
     
     	
     
     
     
        $               5          0          1                            	   2          &   +      )          "                 4                .       #   ,                         /         !   *      %       3   '   
                 (      -          Apr April Aug August Clear Copy Dec December Error while loading page: Feb February Fr Fri Friday Jan January Jul July Jun June Mar March May Mo Mon Monday Nov November Oct October Sa Sat Saturday Sep September Sort this column Su Sun Sunday Th The request for machine translation has failed: The request for machine translation using %s has failed: There are some unsaved changes, are you sure you want to leave? Thu Thursday Today Tu Tue Tuesday We Wed Wednesday Project-Id-Version: Weblate 1.2
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2015-03-11 10:58+0200
Last-Translator: Åke Engelbrektson <eson@svenskasprakfiler.se>
Language-Team: Swedish <https://hosted.weblate.org/projects/weblate/javascript/sv/>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 2.3-dev
 Apr April Aug Augusti Rensa Kopiera Dec December Fel vid inläsning av sida: Feb Februari Fr Fre Fredag Jan Januari Jul Juli Jun Juni Mar Mars Maj Må Mån Måndag Nov November Okt Oktober Lö Lör Lördag Sep September Sortera denna kolumn Sö Söndag Söndag To Begäran om maskinöversättning misslyckades: Begäran om maskinöversättning med %s misslyckades: Det finns osparade ändringar, är du säker att du vill överge dessa? Tor Torsdag I dag Ti Tis Tisdag On Ons Onsdag 