��    C      4  Y   L      �  #   �  ,   �               (     F     Z     j     r     �  
   �     �     �     �     �     �     �          +     ?     R     k     {  	   �     �     �  '   �     �     �          -  $   M  '   r     �     �     �     �  /   �     	     	     !	     0	     L	     i	     �	     �	     �	     �	     �	     
  ?   ,
     l
     �
     �
     �
     �
     �
     �
     �
     �
     �
  
     	             7     I  �  W  3   "  E   V     �     �  )   �     �          /     ;  "   W     z     �     �     �  "   �  0   �       )   7     a     �  '   �     �     �     �     �  
     3     4   A  B   v  -   �  7   �  3     A   S  3   �     �     �     �  1        4     S     `  
   w     �     �     �     �     �     �  #   �  %   �  �   �     w     �     �     �     �  %   �     �     �          +     I     [  0   l     �     �     0   #          *       ?           B   8       =       $       -          5      )                9      %         
             @   2                   ;   A                     4   6   (      :      +                	                     !       7   >      ,       '      /   .          <          1   C   "   3       &    Activation email will be sent here. All recent changes made using Weblate in %s. Allowed hosts Android String Resource At least six characters long. Automatic detection Change password Contact Created new SSH key. Database backend Debug mode Django caching E-mail Email addresses Failed to generate key: %s Federated avatar support Gettext PO file Gettext PO file (monolingual) Indexing offloading Interface Language Invalid host name given! Java Properties Java Properties (UTF-8) Languages Login Message Message has been sent to administrator. Notification on any translation Notification on merge failure Notification on new comment Notification on new contributor Notification on new language request Notification on new string to translate Notification on new suggestion OS X Strings PHP strings Password Password needs to have at least six characters. Password reset Plural Plural form %d Plural form descriptionFew Plural form descriptionMany Plural form descriptionOne Plural form descriptionOther Plural form descriptionThree Plural form descriptionTwo Plural form descriptionZero Qt Linguist Translation File Recent changes in %s Repeat the password so we can verify you typed it in correctly. Secondary languages Singular Site administrator Site domain Subject Subscribed projects User profile User registration Username XLIFF Translation File Your email Your name Your profile has been updated. [hostname hashed] pyuca library Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2015-05-21 16:03+0200
PO-Revision-Date: 2013-08-16 18:04+0200
Last-Translator: anh phan <ppanhh@gmail.com>
Language-Team: Vietnamese <http://hosted.weblate.org/projects/weblate/master/vi/>
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 1.7-dev
 Email kích hoạt sẽ được gửi tại đây. Tất cả các thay đổi gần đây sử dụng Weblate trong %s. Các host được cho phép Android String Resource Ít nhất sáu ký tự về độ dài. Tự động nhận diện Thay đổi mật khẩu Liên lạc Đã tạo mới khóa SSH. Cơ sở dữ liệu đầu cuối Chế độ gỡ rối Bộ nhớ đệm cho Django Email Địa chỉ email Xảy ra lỗi khi tạo khóa: %s Hỗ trợ hình ảnh đại diện tổ chức Tập tin kiểu Gettext PO Tập tin kiểu Gettext PO (đơn ngữ) Giảm tải đánh chỉ số Ngôn ngữ giao diện Tên host cung cấp không hợp lệ! Java Properties Java Properties (UTF-8) Ngôn ngữ Đăng nhập Tin nhắn Đã gửi tin nhắn đến người quản trị. Nhận thông báo về bất kỳ bản dịch nào Nhận thông báo khi xảy ra lỗi lúc kết nối dữ liệu Nhận thông báo khi có bình luận mới Nhận thông báo khi có người cống hiến mới Nhận thông báo khi có lời đề nghị mới Nhận thông báo khi phần dịch bổ sung các chuỗi mới Nhận thông báo khi có lời đề nghị mới Chuỗi hệ điều hành OS X PHP strings Mật khẩu Mật khẩu phải có ít nhất sáu ký tự. Cài đặt lại mật khẩu Số nhiều Dạng số nhiều %d Một vài Nhiều Một Khác Ba Hai Rỗng Tập tin dịch kiểu Qt Linguist Các thay đổi gần đây trong %s Lặp lại mật khẩu để chúng tôi có thể xác nhận rằng bạn đã điền vào mật khẩu đúng theo ý của bạn. Ngôn ngữ thứ hai Số ít Trang quản trị Tên miền Chủ đề Chủ đề đã đăng ký theo dõi Hồ sơ người dùng Đăng ký người dùng Tên tài khoản Tập tin dịch kiểu XLIFF Email của bạn Tên của bạn Hồ sơ của bạn đã được cập nhật. [mã hash của tên host] Thư viện pyuca 