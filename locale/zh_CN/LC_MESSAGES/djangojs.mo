��          �            h  -   i     �     �     �     �  
   �     �  ;   �          )  /   I  8   y  ?   �     �  �    !   �                     $     1     A  -   H     v     �     �  %   �  0   �                        
                                               	              AJAX request to load this content has failed! Cancel Confirm resetting repository Copy Fuzzy strings Loading… Ok Resetting the repository will throw away all local changes! Sort this column Strings with any failing checks The request for machine translation has failed: The request for machine translation using %s has failed: There are some unsaved changes, are you sure you want to leave? Translated strings Project-Id-Version: Weblate 1.2
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2014-05-06 13:05+0200
PO-Revision-Date: 2014-02-14 01:35+0200
Last-Translator: Huang Zhiyi <hzy980512@126.com>
Language-Team: Simplified Chinese <http://l10n.cihar.com/projects/weblate/javascript/zh_CN/>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 1.9-dev
 通过 AJAX 加载内容失败！ 取消 确认代码库重设 复制 模糊条目 正在加载… 确定 重设代码库将丢失所有本地修改！ 按该列排序 未通过检查的条目 请求机器翻译失败： 请求使用 %s 机器翻译失败： 您有未保存的更改，确定要离开么？ 已翻译条目 