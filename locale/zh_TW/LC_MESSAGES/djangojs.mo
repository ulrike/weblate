��          �            h  -   i     �     �     �     �  
   �     �  ;   �          )  /   I  8   y  ?   �     �  �    )   �               $     +     ;     H  -   O     }     �  $   �  .   �  <   �     :                   
                                               	              AJAX request to load this content has failed! Cancel Confirm resetting repository Copy Fuzzy strings Loading… Ok Resetting the repository will throw away all local changes! Sort this column Strings with any failing checks The request for machine translation has failed: The request for machine translation using %s has failed: There are some unsaved changes, are you sure you want to leave? Translated strings Project-Id-Version: Weblate 1.2
Report-Msgid-Bugs-To: weblate@lists.cihar.com
POT-Creation-Date: 2014-05-06 13:05+0200
PO-Revision-Date: 2014-05-09 19:49+0200
Last-Translator: ezjerry liao <ezjerry@gmail.com>
Language-Team: Traditional Chinese <https://l10n.cihar.com/projects/weblate/javascript/zh_TW/>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 1.9
 AJAX 請求載入此內容已經失敗！ 取消 確認重設資源庫 複製 模糊的字串 載入中… 確定 重設資源庫將遺失所有本地更改！ 重新排列 查核字串與任何的缺點 讓機器翻譯的請求已失敗： 讓機器翻譯使用 %s 的請求已失敗： 還有一些尚未儲存的變更，您確定要離開嗎？ 已翻譯的字串 